var data = [
  {
    "name": "Test	Task #1",
    "date": "12/01/2012",
    "assigned": "John	Doe"
  },
  {
    "name": "Test	Task #2",
    "date": "12/02/2012",
    "assigned": "John	Doe"
  },
  {
    "name": "Test	Task #3",
    "date": "12/03/2012",
    "assigned": "John	Doe"
  },
  {
    "name": "Test	Task #4",
    "date": "12/04/2012",
    "assigned": "John	Doe"
  },
  {
    "name": "Test	Task #5",
    "date": "12/05/2012",
    "assigned": "John	Doe"
  },
  {
    "name": "Test	Task #6",
    "date": "12/06/2012",
    "assigned": "John Doe"
  },
  {
    "name": "Test	Task #7",
    "date": "12/07/2012",
    "assigned": "John	Doe"
  }
];

function drawTable() {
  for (i = 0; i < data.length; i++) {
    $('tbody').append("<tr><td><b>" + data[i].name + "</b></td><td> " + data[i].date + "</td><td><span class='slideRight'>" + data[i].assigned + "</span></td></tr>");
  }
}

$("#submit").click(function () {
  //checking if all the three values exists
  if ($("#name").val() && $("#date").val() && $("#assignedTo").val()) {
    var valid = new RegExp(/^[0-9]{2}[/][0-9]{2}[/][0-9]{4}$/);
    //checking for a valid date
    if (valid.exec($("#date").val())) {
      const obj = {
        "name": $("#name").val(),
        "date": $("#date").val(),
        "assigned": $("#assignedTo").val()
      }
      //prepending the task object to the data
      data.unshift(obj);
      //data = [obj, ...data]
      //console.log(data);
      //empty the whole table body and redraw the table
      //Another approach is just prepending the template string to the DOM.
      // 
      $('tbody').empty();
      $("#name").val("");
      $("#date").val("");
      $("#assignedTo").val("");
      drawTable();
    }
    else {
      alert("Invalid date format.\nDate must be mm/dd/yyyy")
    }
  }
  else {
    if (!$("#assignedTo").val() && !$("#date").val() && (!$("#name").val())) {
      alert("Form can't be empty");
    }
    if (!$("#assignedTo").val()) {
      $("#assignedTo").focus();
    }
    if (!$("#date").val()) {
      $("#date").focus();
    }
    if (!$("#name").val()) {
      $("#name").focus();
    }
  }
})

//load the data into the table whenever the document is ready
$("document").ready(function () {
  drawTable();
})